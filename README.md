# Learning Rust

The purpose of this Repo is to contain my practice with the Rust Programming Language.
I'll be following the [Rust Book](https://doc.rust-lang.org/book/) and probably creating a small project with every single chapter to practice the concepts contained.